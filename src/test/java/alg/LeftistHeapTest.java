package alg;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LeftistHeapTest {
    @Test
    void insertKeyTest() {
        final LeftistHeap heap = new LeftistHeap();
        assertNull(heap.getRoot());

        heap.insert(4);
        assertEquals(heap.getRoot().getKey(), 4);

        heap.insert(1);
        heap.insert(13);
        heap.insert(5);
        assertEquals(heap.getRoot().getKey(), 1);
        assertEquals(heap.getRoot().getLeft().getKey(), 4);
        assertEquals(heap.getRoot().getRight().getKey(), 5);
        assertEquals(heap.getRoot().getRight().getLeft().getKey(), 13);
    }

    @Test
    void insertDistTest() {
        final LeftistHeap heap = new LeftistHeap();
        assertNull(heap.getRoot());

        heap.insert(4);
        assertEquals(heap.getRoot().getDist(), 0);

        heap.insert(1);
        heap.insert(13);
        heap.insert(5);
        heap.insert(212);
        assertEquals(heap.getRoot().getDist(), 1);
        assertEquals(heap.getRoot().getLeft().getDist(), 1);
        assertEquals(heap.getRoot().getRight().getDist(), 0);
        assertEquals(heap.getRoot().getLeft().getLeft().getDist(), 0);
        assertEquals(heap.getRoot().getLeft().getRight().getDist(), 0);
    }

    @Test
    void getMinTest() {
        final LeftistHeap heap = new LeftistHeap();

        heap.insert(6);
        assertEquals(heap.getMin(), 6);
        heap.insert(7);
        assertEquals(heap.getMin(), 6);
        heap.insert(3);
        assertEquals(heap.getMin(), 3);
        heap.insert(3);
        assertEquals(heap.getMin(), 3);
        heap.insert(4);
        assertEquals(heap.getMin(), 3);
        heap.insert(1);
        assertEquals(heap.getMin(), 1);
        heap.insert(-1);
        assertEquals(heap.getMin(), -1);
        heap.insert(-10_000);
        assertEquals(heap.getMin(), -10_000);
    }


    static Stream<Arguments> lists() {
        return Stream.of (
                Arguments.of(List.of()),
                Arguments.of(List.of(1)),
                Arguments.of(List.of(-1)),
                Arguments.of(List.of(1, 2, 3, 4, 5)),
                Arguments.of(List.of(4, 7, 3, 1, 8, 3, 8, 2)),
                Arguments.of(List.of(0, 0, 0, 0, 0)),
                Arguments.of(List.of(1, 0, 1, 0, 1, 0, 1, 0)),
                Arguments.of(List.of(9, 8, 7, 6, 5, 4, 3, 2, 1)),
                Arguments.of(List.of(1, 9, 2, 8, 3, 7, 4, 6, 5))
        );
    }

    @ParameterizedTest
    @MethodSource("lists")
    void sortTest(final List<Integer> list) {
        final List<Integer> actual = list.stream().sorted().toList();
        final List<Integer> expected = new ArrayList<>();

        final LeftistHeap heap = new LeftistHeap();
        for (final Integer e: list) {
            heap.insert(e);
        }
        while (heap.getRoot() != null) {
            expected.add(heap.extractMin());
        }

        assertEquals(actual, expected);
    }



}