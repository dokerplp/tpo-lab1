package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FishObjectTest {

    @Test
    void colorTest() {
        final String color = "golden";
        final FishObject f = new FishObject(color);

        assertEquals(f.getColor(), color);
    }

    @Test
    void actionTest() {
        final String color = "golden";
        final FishObject f = new FishObject(color);
        final String action = "swimming";

        f.setAction(action);

        assertEquals(f.getAction(), action);
        assertEquals(f.toString(), "swimming golden fish");
    }
}