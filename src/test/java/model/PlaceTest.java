package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlaceTest {

    @Test
    void addHuman() {
        final Place p = new Place();
        final Human h = new Human("a", true);

        assertEquals(p.getHumans().size(), 0);

        p.addHuman(h);

        assertEquals(p.getHumans().size(), 1);
        assertEquals(p.getHumans().get(0).getName(), "a");
    }

    @Test
    void addObject() {
        final Place p = new Place();
        final String obj = "o";

        assertEquals(p.getObjects().size(), 0);

        p.addObject(obj);

        assertEquals(p.getObjects().size(), 1);
        assertEquals(p.getObjects().get(0), obj);
    }

    @Test
    void toStringTest() {
        final Place place = new Place();
        final Human h = new Human("Abobus", false);
        final String o = "apple";

        assertEquals("People: [], Around: []", place.toString());

        place.addHuman(h);
        place.addObject(o);

        assertEquals("People: [Abobus sees nothing and not blinks, holds nothing in her hands, wishes nothing and do nothing], Around: [apple]", place.toString());
    }
}