package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void seeTest() {
        final Human h = new Human("Abobus", true);
        final String image = "sun";

        h.see(image);

        assertEquals(h.getHumanEyes().getImage(), image);
    }

    @Test
    void keepInHandsTest() {
        final Human h = new Human("Abobus", true);
        final String obj = "pineapple";

        h.hold(obj);

        assertEquals(h.getHumanHands().getObject(), obj);
    }

    @Test
    void wishesTest() {
        final Human h = new Human("Abobus", true);

        assertEquals(h.getWishes().size(), 0);

        h.addWish("go to sleep");

        assertEquals(h.getWishes().size(), 1);
        assertEquals(h.getWishes().get(0), "go to sleep");
    }

    @Test
    void getName() {
        final String name = "Abobus";
        final Human h = new Human(name, true);

        assertEquals(h.getName(), name);
    }

    @Test
    void eyesTest() {
        final Human h = new Human("Abobus", true);

        h.getHumanEyes().setBlinking(true);

        assertTrue(h.getHumanEyes().isBlinking());

        h.getHumanEyes().setImage("Coffee");

        assertEquals(h.getHumanEyes().getImage(), "Coffee");
    }

    @Test
    void actTest() {
        final Human h = new Human("Abobus", true);
        final String action = "jump";

        h.act(action);

        assertEquals(h.getAction(), action);
    }

    @Test
    void isManTest() {
        final Human h = new Human("Abobus", true);

        assertTrue(h.isMan());
    }

    @Test
    void toStringTest() {
        final Human h1 = new Human("Abobus", true);
        h1.see("apple");
        h1.hold("pineapple");
        h1.getHumanEyes().setBlinking(true);
        h1.act("sleeps");

        String actual = "Abobus sees apple and  blinks, holds pineapple in his hands, wishes nothing and sleeps";
        String expected = h1.toString();

        assertEquals(actual, expected);

        final Human h2 = new Human("Abobus", false);
        h2.addWish("lemon");

        actual = "Abobus sees nothing and not blinks, holds nothing in her hands, wishes [lemon] and do nothing";
        expected = h2.toString();

        assertEquals(actual, expected);
    }
}