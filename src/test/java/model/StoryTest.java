package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StoryTest {

    @Test
    void storyTest() {
        final Story story = new Story();
        final String text = "People: [Ford sees nothing and not blinks, holds swimming and shimmering with colors golden fish in his hands, wishes nothing and do nothing, Arthur sees Ford and  blinks, holds nothing in his hands, wishes [something simple and familiar, something for which one could mentally cling, to see a bag of corn flakes] and do nothing], Around: [Dentrassi underwear, squarish mattresses, man from Betelgeuse sees nothing and not blinks, holds nothing in his hands, wishes nothing and holding a small fish and offering to put it in his ear]";

        assertEquals(story.story(), text);
    }
}