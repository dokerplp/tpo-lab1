package func;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.*;

class FunctionTest {

    private static final double ACCURACY = 0.000_01;
    private static final Function FUNCTION = new Function(ACCURACY);

    static Stream<Arguments> secTestArgs() {
        return Stream.of (
                Arguments.of(0, 1),
                Arguments.of(PI / 4, sqrt(2)),
                Arguments.of(PI / 3, 2),
                Arguments.of(2 * PI / 3, -2),
                Arguments.of(3 * PI / 4, -sqrt(2)),

                Arguments.of(PI, -1),
                Arguments.of(PI + PI / 4, -sqrt(2)),
                Arguments.of(PI + PI / 3, -2),
                Arguments.of(PI + 2 * PI / 3, 2),
                Arguments.of(PI + 3 * PI / 4, sqrt(2)),

                Arguments.of(2 * PI, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("secTestArgs")
    void secTest(final double x, final double expected) {
        assertEquals(expected, FUNCTION.sec(x), ACCURACY);
    }

}