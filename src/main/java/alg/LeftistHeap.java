package alg;

import lombok.Getter;

public class LeftistHeap {

    @Getter
    private LeftistHeapNode root;


    @Getter
    public static class LeftistHeapNode {
        private LeftistHeapNode left;
        private LeftistHeapNode right;
        private final int key;
        private int dist;

        public LeftistHeapNode(final int key) {
            this.key = key;
        }

        private static void swapChildren(final LeftistHeapNode a) {
            final LeftistHeapNode t = a.left;
            a.left = a.right;
            a.right = t;
        }

        private static LeftistHeapNode merge(final LeftistHeapNode a, final LeftistHeapNode b) {
            if (a == null) {
                return b;
            } else if (b == null) {
                return a;
            } else if (a.key < b.key) {
                return merge1(a, b);
            } else {
                return merge1(b, a);
            }
        }

        private static LeftistHeapNode merge1(final LeftistHeapNode a, final LeftistHeapNode b) {
            if (a.left == null) {
                a.left = b;
            } else {
                a.right = merge(a.right, b);
                if (a.left.dist < a.right.dist) {
                    swapChildren(a);
                }
                a.dist = a.right.dist + 1;
            }
            return a;
        }
    }

    public void insert(final int x) {
        root = LeftistHeapNode.merge(new LeftistHeapNode(x), root);
    }

    public int getMin() {
        return root.key;
    }

    public int extractMin() {
        final int ret = root.key;
        root = LeftistHeapNode.merge(root.left, root.right);
        return ret;
    }
}
