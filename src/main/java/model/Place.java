package model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Place {

    @Getter
    private final List<Human> humans;

    @Getter
    private final List<String> objects;

    public Place() {
        this.humans = new ArrayList<>();
        this.objects = new ArrayList<>();
    }

    public void addHuman(final Human h) {
        humans.add(h);
    }

    public void addObject(final String o) {
        objects.add(o);
    }

    @Override
    public String toString() {
        final String humansDesc = humans.toString();
        final String objectsDesc = objects.toString();
        return String.format("People: %s, Around: %s", humansDesc, objectsDesc);
    }
}
