package model;

public class Story {
    public String story() {
        final Human ford = new Human("Ford", true);
        final Human arthur = new Human("Arthur", true);
        final Human smbdy = new Human("man from Betelgeuse", true);
        final FishObject f = new FishObject("golden");
        final Place place = new Place();

        f.setAction("swimming and shimmering with colors");
        ford.hold(f.toString());
        arthur.see("Ford");
        arthur.getHumanEyes().setBlinking(true);
        arthur.addWish("something simple and familiar");
        arthur.addWish("something for which one could mentally cling");
        place.addHuman(ford);
        place.addHuman(arthur);
        place.addObject("Dentrassi underwear");
        place.addObject("squarish mattresses");


        smbdy.act("holding a small fish and offering to put it in his ear");
        place.addObject(smbdy.toString());

        arthur.addWish("to see a bag of corn flakes");
        return place.toString();
    }
}
