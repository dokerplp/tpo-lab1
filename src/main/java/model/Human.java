package model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Human {

    private final String name;
    private final HumanEyes humanEyes;
    private final HumanHands humanHands;
    private final List<String> wishes;
    private final boolean isMan;

    @Getter
    private String action;

    public Human(final String name, final boolean isMan) {
        this.name = name;
        this.isMan = isMan;
        this.humanEyes = new HumanEyes();
        this.humanHands = new HumanHands();
        this.wishes = new ArrayList<>();
    }

    public void see(final String image) {
        humanEyes.image = image;
    }

    public void hold(final String o) {
        humanHands.object = o;
    }

    public void addWish(final String wish) {
        wishes.add(wish);
    }

    public void act(final String action) {
        this.action = action;
    }

    @Getter
    @Setter
    public static class HumanEyes {
        private String image;
        private boolean isBlinking;
    }

    @Getter
    public static class HumanHands {
        private String object;
    }

    @Override
    public String toString() {
        return String.format("%s sees %s and %s blinks, holds %s in %s hands, wishes %s and %s",
                name,
                humanEyes.image == null ? "nothing" : humanEyes.image,
                humanEyes.isBlinking ? "" : "not",
                humanHands.object == null ? "nothing" : humanHands.object,
                isMan ? "his" : "her",
                wishes.isEmpty() ? "nothing" : wishes,
                action == null ? "do nothing" : action
                );
    }
}