package model;

import lombok.Getter;
import lombok.Setter;

public class FishObject {
    @Getter
    private final String color;

    @Getter
    @Setter
    private String action;

    public FishObject(final String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("%s %s fish", action, color);
    }
}
