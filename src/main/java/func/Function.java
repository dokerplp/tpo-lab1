package func;

public class Function {
    private final double accuracy;

    public Function(final double accuracy) {
        this.accuracy = accuracy;
    }

    public double sec(final double x) {
        double acc = 0;
        int mul = 1;
        int pow = 0;
        double fact = 1;
        double p1 = Double.MAX_VALUE;
        double p2 = 0;
        while (Math.abs(p1 - p2) > accuracy) {
            acc += p2;
            p1 = p2;
            p2 = mul * Math.pow(x, pow) / fact;
            mul *= -1;
            fact *= ++pow;
            fact *= ++pow;
        }
        return 1 / acc;
    }
}
